console.log("Test Entry Point: Working...");


//////////////////////////////////////////////////////////
//////////////////Library Stuff///////////////////////////

//Get the Express Libaray
var express = require("express");
var app = express();

var bodyParser = require("body-parser");

//Find css in "public" folder
app.use(express.static("public"));
app.use(bodyParser.urlencoded({extended: true })); // For posting string working
app.engine('html', require('ejs').renderFile);
app.set("view engine", "ejs", "html"); //Default find .ejs file

//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////
///////////////////// Data structure//////////////////////
var urlToCodeDict = {};
var codeToUrlDict = {};
//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////
//////////////////Helper function////////////////////////
function getMyCode(len) {
    let alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    let results = '';
    for (let i = 0; i < len; i++) {
        results += alphabet[Math.floor(Math.random() * 62)];
    }
    return results;
}

function Encode(longUrl) {
    while (urlToCodeDict[longUrl] === undefined) {
        let code = getMyCode(6);
        if (codeToUrlDict[code] === undefined) {// check if the code is taken!
            codeToUrlDict[code] = longUrl;
            urlToCodeDict[longUrl] = code;
        }
    }
    return urlToCodeDict[longUrl];
}

function Decode(shortUrl) {
    return codeToUrlDict[shortUrl];
}

/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////





//////////////////////////////////////////////////////////
////////////////////GET Request///////////////////////////
/**
 * Home Page
 */
app.get("/", function(req,res){
	//render is only for html5 res.render("filename")
	//res.send("Hello World!");
	res.render("homepage");
});


/**
 * Redirection
 */
app.get("/:shortURL", function(req, res){ //htpp://local:host:3000/000001 ---Not dealt with "/:A/:B"
	var shortUrl = req.params.shortURL; //requst --> string: shortUrl ***000001*** 如果是 “／：A” 则req.params.A
	console.log(shortUrl);
	res.redirect(codeToUrlDict[shortUrl]);
});


/**
 * Corner case: When no logic found, then "Page not found"
 */
app.get("*", function(req,res){ //If no logic deal with, then "Page not found"
	res.send("Opzss...Page not found")
});
//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////




//////////////////////////////////////////////////////////
///////////////////POST Request///////////////////////////
/**
 * Assign Short URL
 */
app.post("/urls", function(req, res){
	//
	var originalUrl = req.body.originalURL;
	var customizedUrl = req.body.customizedURL;

	if (originalUrl == "") {
		res.render("homepage");
	} else if (urlToCodeDict[originalUrl] != undefined) {
		res.render("longCodeExist",  {shortUrl : urlToCodeDict[originalUrl], originalUrl : originalUrl});
	}


	if (customizedUrl == "") { //Randomly Assign a new Code
		var shortURL = Encode(originalUrl)
		res.render("resultpage", {shortUrl : shortURL, originalUrl : originalUrl});
	} else {
		if (codeToUrlDict[customizedUrl] === undefined) {
			codeToUrlDict[customizedUrl] = originalUrl;
            urlToCodeDict[originalUrl] = customizedUrl;
			res.render("resultpage", {shortUrl : customizedUrl, originalUrl : originalUrl});
		} else {
			res.render("shortCodeExist", {shortUrl : "Already Exists!!! Please Try Again!!!", originalUrl : originalUrl});
		}
	}

});
//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////



//For Oracle Cloud
// var PORT = process.env.PORT || 8089;
// app.listen(PORT, function() {
// 	console.log("Serving on Port 3000");
// });


// //Listen 
app.listen(3000, function() {
	console.log("Serving on Port 3000");
});